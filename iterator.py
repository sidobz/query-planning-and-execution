#import pandas as pd
import timeit
import re
class iterator(object):
	"""docstring for ClassName"""
	def __init__(self,child ):
		self.child = child
		self.state = "Off"
	def Open(self) :

		return True
	def GetNext(self) :
		return True

	def Close(self) :
		return True

		
class TableScanIterator(iterator):
	def __init__(self, path):
		super(TableScanIterator, self).__init__(path)
		self.header = False
		self.file = False
		self.pointer = 0
		self.output = False
		
		
	
	def Open(self) :
		
		self.state = "On"
		self.file = open(self.child, 'r+')
		print("Open file" ,self.file)
		
		
		return True
	def GetNext(self) :
		line = False

		if self.pointer ==0 : 
			# Get the first row (Header)
			self.header = self.file.readline().split(',')
			self.header[-1] = self.header[-1].replace("\n","")
			#self.child.seek(len(self.header))
			#print ("reading the first row !!!" , self.header)
		 
		# Get the one line from row 1 to the end of the file
		line = self.file.readline()
		#print ("reading a line from data !!!" , line)		
		if line : 
			
			output = line.split(',')
			print("given line form table",output)
			output[-1] = output[-1].replace("\n","")
			# build a dictionnary to do more better the selection after
			dic = {}
			key = 0
			#print("header" ,self.header)
			#print("given line form table",output)
			while key< len(output) : 
				dic[self.header[key]] = output[key]
				key += 1
			self.output = dic
			self.pointer+=1
		# IF that don't have a row from child, return NotFound
		else :
					
			print("Line NotFound")
			self.output = False
			self.pointer = False
			self.state = "Off"
			
		#print("the res from Scan" ,self.output)
		return self.output



	


	def Close(self) :
		self.child.close()
		self.state = "Off"
		return True	





class ProjectIterator(iterator):
	def __init__(self , child,attributs):
		super().__init__(child)
		self.attributs = attributs
		self.output = []

	
	def Open(self ) :
		self.state = "On"
		self.child.Open() 
		
		
		return True



	def GetNext(self) :
		# Get the row from the sub iterator in the dictionnary form
		get_row = self.child.GetNext()
		if get_row :
			l = []
			for key,value in get_row.items():
				if key in self.attributs:
					l.append(value+',')
			if l :
				l[-1] = l[-1]+"\n" 
				self.output = l
		else :
			self.output = False
		if self.child.state =="Off" :
			self.state ="Off"
			print("Off from Projection")
		return self.output
	
	def Close(self) :
		#print(self.child)
		self.child.Close()
		self.state = "Off"
		#print(self.child)
		return True	




class FilterIterator(iterator):
	def __init__(self,  child,conditions ):
		super().__init__(child)
		self.conditions = conditions
		self.output = []
		
	def Open(self) :
		
		# initialize the childScan Iterator
		self.child.Open()
		self.state = "On"
		
		
		return True



	def GetNext(self) :
		# Get the row from the sub iterator in the dictionnary form
		get_row = self.child.GetNext()
		print("get row" ,get_row)
		if get_row :
			
			if self.conditions :
				# split the conditions
				accepted  = False
				for cond in self.conditions : 
					cnd = re.split('=|>', cond)
					
					#verifying if the operator is '=' or '>'
					if '=' in cond and cnd[0] in get_row.keys(): 
						if get_row[cnd[0]] == cnd[1] :
							accepted = True 
					elif '>' in cond and cnd[0] in get_row.keys():
						if get_row[cnd[0]] > cnd[1] :
							accepted = True 
				if accepted :
					self.output = get_row
					print("Yes accepted")
				else :
					self.output = False
					print("Not  accepted")


		if self.child.state =="Off" :
			self.state ="Off"
			print("Off from Filter")
			
		return self.output

	def Close(self) :
		#print(self.child)
		self.child.Close()
		self.state = "Off"
		#print(self.child)
		return True	






class NestedLoopIterator():
	def __init__(self, child , child2 , key_fk ):
		super(NestedLoopIterator, self).__init__(child)
		self.right_child = child2
		self.output = []
		self.key_fk = key_fk
	
	def Open(self) :
		
		# initialize the childScan Iterator
		self.child.Open()
		self.right_child.Open()
		self.state = "On"
		
		
		
		return True



	def GetNext(self) :
		# Get the row from the sub iterator in the dictionnary form
		left_get_row = self.child.GetNext()
		right_get_row = self.right_child.GetNext()
		if left_get_row and right_get_row :
			# Split the join condition 
			cn = self.key_fk.split('=')
			if self.key_fk :
			# Process the join with verify the condition join between the 2 rows
				l = []
			for key,value in get_row.items():
				if key in self.list_attributs:
					l.append(value)
			if l : 
				self.output = l
		else :

			self.output = []
			print("NotFount")
			self.state = False
		return self.output

	def Close(self) :
		#print(self.child)
		self.child.Close()
		#print(self.child)
		return True	


