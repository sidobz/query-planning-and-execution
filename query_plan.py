from iterator import ProjectIterator ,TableScanIterator ,FilterIterator ,NestedLoopIterator
import timeit
import os

# Add the header for all csv files in the first above

def insert(originalfile,string):
    with open(originalfile,'r') as f:
        with open('newfile.txt','w') as f2: 
            f2.write(string+'\n')
            f2.write(f.read())
    os.rename('newfile.txt',originalfile)
'''
insert('info_type.csv' , "id,info")
insert('company_name.csv' , "id,name,country_code")
insert('info_type.csv' , "id,info")
insert('info_type.csv' , "id,info")
insert('info_type.csv' , "id,info")
insert('info_type.csv' , "id,info")
insert('info_type.csv' , "id,info")
insert('info_type.csv' , "id,info")
'''		
# f.write(line.rstrip('\r\n') + '\n' + header)



#pi = ProjectIterator('info_type.csv' , "info")
#pi.Open()
#print(pi.GetNext())
#pi.Close()
csv_files  = ["company_name.csv","movie_companies.csv" ]


def query_1_execution_plan(csv_files):

	# initialize the project Iterator
	
	table_scan = TableScanIterator(csv_files[0])
	selection = FilterIterator(table_scan,["country_code=[de]"])
	selection.Open()
	table_scan = TableScanIterator(csv_files[1])
	nested_loop = NestedLoopIterator(selection , table_scan , "id=company_id")
	project_iterator = ProjectIterator( nested_loop,["id","company_id"])	
	project_iterator.Open()
	
	
	
	
	# Open the project iterator for openning all the arguments and data that needed
	
	
	
	
	#print("row" , project_iterator.GetNext())

	


	
	result = open("result.csv", 'w')
	while project_iterator.state == "On"  :
		items  =project_iterator.GetNext()
		if items :
			for i in items : 
				result.write(i)
	
		

	# return the result of the query to a new csv file
	return True

print ("query_1_execution_plan!!!!")
query_1_execution_plan(csv_files)

"""

fi = FilterIterator(csv_files, )
pi.Open()
print(pi.GetNext())
pi.Close()
"""


#tb = TableScanIterator('info_type.csv')
#tb.Open()
#print(f"Execution time is: {timeit.timeit()}")

#while tb.pointer  :
#	print ("items  :",tb.GetNext())
	
#print(f"Execution time is: {timeit.timeit()}")	
#tb.Close()